package az.ingress.axon.cart.observer;

public class AzerObserver extends Observer {

    public AzerObserver(Subject subject) {
        this.subject = subject;
        this.subject.subscribe(this);
    }

    @Override
    public void update() {
        this.subject = subject;
        System.out.println("Observer recived update..." + subject.getState() + "  " + this.getClass());
    }
}
