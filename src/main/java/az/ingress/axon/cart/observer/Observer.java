package az.ingress.axon.cart.observer;

public abstract class Observer {

    protected Subject subject;

    public abstract void update();
}
