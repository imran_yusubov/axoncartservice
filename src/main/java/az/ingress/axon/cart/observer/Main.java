package az.ingress.axon.cart.observer;

public class Main {

    public static void main(String[] args) {
        Subject subject = new Subject();
        System.out.println("Updating state 1");
        subject.setState(100);

        Observer farid = new FaridObserver(subject);
        System.out.println("Updating state 2");
        subject.setState(200);

        Observer azer = new AzerObserver(subject);
        System.out.println("First state change: 15");
        subject.setState(15);

        subject.unsubscribe(farid);
        System.out.println("Second state change: 10");
        subject.setState(10);
    }
}
