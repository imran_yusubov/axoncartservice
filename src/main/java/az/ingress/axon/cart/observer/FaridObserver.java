package az.ingress.axon.cart.observer;

import lombok.extern.java.Log;

@Log
public class FaridObserver extends Observer {

    public FaridObserver(Subject subject) {
        this.subject = subject;
        this.subject.subscribe(this);
    }


    @Override
    public void update() {
        System.out.println("Observer recived update..." + subject.getState() + "  " + this.getClass());
    }
}
