package az.ingress.axon.cart.projector;

import az.ingress.axon.cart.coreapi.domain.FoodCart;
import az.ingress.axon.cart.coreapi.events.FoodCartCreatedEvent;
import az.ingress.axon.cart.coreapi.events.ProductDeSelectedEvent;
import az.ingress.axon.cart.coreapi.events.ProductSelectedEvent;
import az.ingress.axon.cart.coreapi.query.FindFoodCartQuery;
import az.ingress.axon.cart.coreapi.repository.FoodCartRepository;
import az.ingress.axon.cart.dto.FoodCartView;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class FoodCartProjector {

    private final FoodCartRepository repository;
    private final ModelMapper mapper;

    @EventHandler
    public void on(FoodCartCreatedEvent event) {
        log.info("FoodCart persisted into database {}", event.getFoodCartId());
        FoodCart foodCart = new FoodCart(event.getFoodCartId(), Collections.emptyMap());
        repository.save(foodCart);
    }

    @EventHandler
    public void on(ProductDeSelectedEvent event) {
        log.info("FoodCart de-select product event persisted into database {}", event.getFoodCartId());
        FoodCart foodCart = repository.findById(event.getFoodCartId()).orElseThrow(() -> new RuntimeException("Food Cart does not exist"));
        Map<UUID, Integer> products = foodCart.getProducts();
        Integer count = products.getOrDefault(event.getProductId(), 0);
        if ((count - event.getQuantity()) <= 0)
            count = 0;
        else
            count = count - event.getQuantity();

        products.put(event.getProductId(),count);
        foodCart.setProducts(products);
        repository.save(foodCart);
    }

    @EventHandler
    public void on(ProductSelectedEvent event) {
        log.info("FoodCart select product event persisted into database {}", event.getFoodCartId());
        FoodCart foodCart = repository.findById(event.getFoodCartId()).orElseThrow(() -> new RuntimeException("Food Cart does not exist"));
        Map<UUID, Integer> products = foodCart.getProducts();
        Integer count = products.getOrDefault(event.getProductId(), 0);
        products.put(event.getProductId(), count + event.getQuantity());
        foodCart.setProducts(products);
        repository.save(foodCart);
    }

    @QueryHandler
    @Transactional
    public FoodCartView handle(FindFoodCartQuery query) {
        log.info("Querying cart with id {}", query.getFoodCartId());
        FoodCart foodCart = repository.findById(query.getFoodCartId()).orElseThrow(() -> new RuntimeException("Food Cart does not exist"));
        return mapper.map(foodCart, FoodCartView.class);
    }

}
