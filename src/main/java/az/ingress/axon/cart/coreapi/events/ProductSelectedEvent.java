package az.ingress.axon.cart.coreapi.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductSelectedEvent {

    private UUID foodCartId;

    private UUID productId;
    private Integer quantity;
}
