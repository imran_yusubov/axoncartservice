package az.ingress.axon.cart.coreapi.events;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class FoodCartCreatedEvent {

    private UUID foodCartId;
}
