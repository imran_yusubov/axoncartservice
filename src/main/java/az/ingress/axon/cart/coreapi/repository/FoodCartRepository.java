package az.ingress.axon.cart.coreapi.repository;

import az.ingress.axon.cart.coreapi.domain.FoodCart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FoodCartRepository extends JpaRepository<FoodCart, UUID> {
}
