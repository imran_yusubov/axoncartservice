package az.ingress.axon.cart.coreapi.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FindFoodCartQuery {

    private UUID foodCartId;
}
