package az.ingress.axon.cart.dto;

import lombok.Data;

import java.util.Map;
import java.util.UUID;

@Data
public class FoodCartView {

    private UUID foodCartId;
    private Map<UUID, Integer> products;
}
