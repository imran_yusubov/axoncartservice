package az.ingress.axon.cart.controllers;

import az.ingress.axon.cart.coreapi.commands.CreateFoodCartCommand;
import az.ingress.axon.cart.coreapi.commands.DeSelectProductCommand;
import az.ingress.axon.cart.coreapi.commands.SelectProductCommand;
import az.ingress.axon.cart.coreapi.query.FindFoodCartQuery;
import az.ingress.axon.cart.dto.FoodCartView;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RestController
@RequestMapping("cart")
@RequiredArgsConstructor
public class FoodOrderController {

    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;

    @PostMapping
    public UUID handle() {
        log.info("Received cart action");
        return commandGateway.sendAndWait(new CreateFoodCartCommand());
    }

    @PostMapping("/add")
    public void handle(@RequestBody SelectProductCommand command) {
        log.info("Received select product action");
        commandGateway.send(command);
    }

    @PostMapping("/remove")
    public void handle(@RequestBody DeSelectProductCommand command) {
        log.info("Received de select product action");
        commandGateway.send(command);
    }

    @GetMapping("{foodCartId}")
    public CompletableFuture<FoodCartView> handle(@PathVariable("foodCartId") String foodCartId) {
        return queryGateway.query(new FindFoodCartQuery(UUID.fromString(foodCartId)),
                ResponseTypes.instanceOf(FoodCartView.class));
    }
}
