package az.ingress.axon.cart.aggregates;

import az.ingress.axon.cart.coreapi.commands.CreateFoodCartCommand;
import az.ingress.axon.cart.coreapi.commands.DeSelectProductCommand;
import az.ingress.axon.cart.coreapi.commands.SelectProductCommand;
import az.ingress.axon.cart.coreapi.events.FoodCartCreatedEvent;
import az.ingress.axon.cart.coreapi.events.ProductDeSelectedEvent;
import az.ingress.axon.cart.coreapi.events.ProductSelectedEvent;
import az.ingress.axon.cart.coreapi.exception.ProductDeSelectedException;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Data
@Aggregate
@NoArgsConstructor
public class FoodCartAggregate {

    @AggregateIdentifier
    private UUID foodCartId;

    private Map<UUID, Integer> selectedProducts;

    @CommandHandler
    public FoodCartAggregate(CreateFoodCartCommand command) {
        log.info("Received create food cart command");
        UUID aggregateId = UUID.randomUUID();
        AggregateLifecycle.apply(new FoodCartCreatedEvent(aggregateId));
    }

    @CommandHandler
    public void handle(SelectProductCommand command) {
        log.info("Received select product command {}", command);
        AggregateLifecycle.apply(ProductSelectedEvent.builder()
                .foodCartId(foodCartId)
                .productId(command.getProductId())
                .quantity(command.getQuantity())
                .build());
    }


    @CommandHandler
    public void handle(DeSelectProductCommand command) {
        log.info("Received de select product command {}", command);
        AggregateLifecycle.apply(ProductDeSelectedEvent.builder()
                .foodCartId(foodCartId)
                .productId(command.getProductId())
                .quantity(command.getQuantity())
                .build());
    }

    @EventSourcingHandler
    public void on(FoodCartCreatedEvent event) {
        log.info("Processing food cart created event {}", event.getFoodCartId());
        foodCartId = event.getFoodCartId();
        selectedProducts = new HashMap<>();
    }

    @EventSourcingHandler
    public void on(ProductSelectedEvent event) {
        log.info("Processing product selection event {}", event);
        selectedProducts.merge(event.getProductId(), event.getQuantity(), Integer::sum);
        log.info("The cart state is {}", this);
    }

    @EventSourcingHandler
    public void on(ProductDeSelectedEvent event) {
        log.info("Processing product de selection event {}", event);
        if (!selectedProducts.containsKey(event.getProductId())) {
            throw new ProductDeSelectedException();
        }


        Integer value = selectedProducts.get(event.getProductId());
        if (value - event.getQuantity() < 0)
            selectedProducts.put(event.getProductId(), 0);
        selectedProducts.put(event.getProductId(), value - event.getQuantity());

        log.info("The cart state is {}", this);
    }

}
